package com.hcl.ecommerce.category.service;

import com.hcl.ecommerce.category.entity.dto.CategoryResponseDto;
import com.hcl.ecommerce.category.entity.dto.ProductResponseDto;

public interface CategorySearchService {

	CategoryResponseDto getCategoryByName(String categoryName);

	CategoryResponseDto getCategoryById(long id);

	ProductResponseDto getProductByCategoryId(long categoryId);

}
