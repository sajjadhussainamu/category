package com.hcl.ecommerce.category.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.ecommerce.category.client.EcommerceClientProducts;
import com.hcl.ecommerce.category.entity.Category;
import com.hcl.ecommerce.category.entity.dto.CategoryDto;
import com.hcl.ecommerce.category.entity.dto.CategoryResponseDto;
import com.hcl.ecommerce.category.entity.dto.ProductResponseDto;
import com.hcl.ecommerce.category.repository.CategorySearchRepository;

@Service
public class CategorySearchServiceImpl implements CategorySearchService {

	@Autowired
	CategorySearchRepository categorySearchRepository;

	@Autowired
	EcommerceClientProducts ecommerceClientProducts;

	@Override
	public CategoryResponseDto getCategoryByName(String categoryName) {
		List<Category> categories = categorySearchRepository.findByCategoryNameContains(categoryName);
		List<CategoryDto> categoryDtos = new ArrayList<>();
		for (Category category : categories) {
			CategoryDto categoryDto = new CategoryDto();
			BeanUtils.copyProperties(category, categoryDto);
			categoryDtos.add(categoryDto);

		}
		CategoryResponseDto categoryResponseDto = new CategoryResponseDto();
		categoryResponseDto.setCategoryDtos(categoryDtos);
		return categoryResponseDto;
	}

	@Override
	public CategoryResponseDto getCategoryById(long id) {

		Category category = categorySearchRepository.findById(id).get();
		CategoryDto categoryDto = new CategoryDto();
		BeanUtils.copyProperties(category, categoryDto);
		List<CategoryDto> categoryDtos = new ArrayList<>();
		categoryDtos.add(categoryDto);
		CategoryResponseDto categoryResponseDto = new CategoryResponseDto();

		categoryResponseDto.setCategoryDtos(categoryDtos);
		return categoryResponseDto;
	}

	@Override
	public ProductResponseDto getProductByCategoryId(long categoryId) {
		ProductResponseDto productResponseDto = ecommerceClientProducts.getProductByCategoryId(categoryId);
		return productResponseDto;
	}

}
