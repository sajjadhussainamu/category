package com.hcl.ecommerce.category.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.ecommerce.category.entity.Category;


@Repository
public interface CategorySearchRepository extends JpaRepository<Category, Long> {

	List<Category> findByCategoryNameContains(String categoryName);

}
