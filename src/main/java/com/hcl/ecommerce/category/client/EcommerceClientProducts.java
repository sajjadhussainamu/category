package com.hcl.ecommerce.category.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.hcl.ecommerce.category.entity.dto.ProductResponseDto;

//@FeignClient(value = "product-service", url = "http://localhost:8092/ecommerce_product/search")
@FeignClient(name = "http://PRODUCT-SERVICE")
public interface EcommerceClientProducts {

	@GetMapping("/ecommerce_product/search/product_by_category_id/{categoryId}")
	public ProductResponseDto getProductByCategoryId(@PathVariable("categoryId") long categoryId);
}
