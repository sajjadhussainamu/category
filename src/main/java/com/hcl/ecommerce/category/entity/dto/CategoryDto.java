package com.hcl.ecommerce.category.entity.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryDto {

	private long id;

	private String categoryName;

}
