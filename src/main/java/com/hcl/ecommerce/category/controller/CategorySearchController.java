package com.hcl.ecommerce.category.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.ecommerce.category.entity.dto.CategoryResponseDto;
import com.hcl.ecommerce.category.entity.dto.ProductResponseDto;
import com.hcl.ecommerce.category.service.CategorySearchService;

@RestController
@RequestMapping("search")
public class CategorySearchController {

	@Autowired
	CategorySearchService searchService;

	@GetMapping("/category_by_name/{name}")
	public CategoryResponseDto getCategoryByName(@PathVariable String name) {
		return searchService.getCategoryByName(name);
	}

	@GetMapping("/category_by_id/{id}")
	public CategoryResponseDto getCategoryById(@PathVariable("id") long id) {
		return searchService.getCategoryById(id);
	}

	@GetMapping("/product_by_category_id/{categoryId}")
	public ProductResponseDto getProductByCategoryId(@PathVariable("categoryId") long categoryId) {

		ProductResponseDto productResponseDto = searchService.getProductByCategoryId(categoryId);
		return productResponseDto;
	}

}
