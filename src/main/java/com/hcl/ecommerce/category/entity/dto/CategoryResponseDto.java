package com.hcl.ecommerce.category.entity.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CategoryResponseDto {

	private List<CategoryDto> categoryDtos = new ArrayList<>();
}
